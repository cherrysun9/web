from typing import Dict, Any
import flask
from flask import Flask
from flask import request
from flask import jsonify
import urllib
from time import gmtime, strftime
from datetime import datetime
import json
import os
import urllib


app = Flask(__name__)
Accessed = dict()

@app.route('/libraries', methods=['GET'])
def libraries():
    token = os.environ.get('APP_TOKEN')
    request_url = 'https://data.austintexas.gov/resource/tc36-hn4j.json?$$app_token=' + token
    r = urllib.request.urlopen(request_url)
    data = json.loads(r.read())
    output = []
    for item in data:
        for k, v in item.items():
            temp = {}
            k_info = {"name": item["name"]}
            p_info = {"phone": item["phone"]}
            a_info = {"human_address": item["address"]["human_address"]}
        temp.update(a_info)
        temp.update(k_info)
        temp.update(p_info)
        output.append(temp)
    result = dict({"libraries": output})
    response = flask.Response(json.dumps(result), mimetype='application/json')
    response.status_code = 200
    return response


@app.route('/libraries/<library_name>', methods=['GET'])
def get_lib(library_name):
    global Accessed
    Etag = flask.request.headers.get("If-None-Match")
    time_compare = flask.request.headers.get("If-Modified-Since" )
    if Etag is not None:
        if Etag in Accessed:
            response = flask.Response()
            response.status_code = 304
            return response

    if time_compare is not None:
        if library_name in Accessed:
            time = datetime.strptime(time_compare, "%a, %d %b %Y %I:%M:%S %Z")
            Ltime = datetime.strptime(Accessed[library_name], "%a, %d %b %Y %I:%M:%S")
            if time >= Ltime:
                response = flask.Response()
                response.status_code = 304
                return response

    token = os.environ.get('APP_TOKEN')
    request_url = 'https://data.austintexas.gov/resource/tc36-hn4j.json?$$app_token=' + token
    r = urllib.request.urlopen(request_url)
    data = json.loads(r.read())
    output = {}
    for item in data:
        for k, v in item.items():
            if v == library_name:
                k_info = {k: v}
                p_info = {"phone": item["phone"]}
                a_info = {"human_address": item["address"]["human_address"]}
                output.update(a_info)
                output.update(k_info)
                output.update(p_info)
                current_time = datetime.utcnow().strftime("%a, %d %b %Y %I:%M:%S")
                Accessed.update({library_name: current_time})

                response = flask.Response(json.dumps(output), mimetype='application/json')
                response.headers["ETag"] = library_name
                response.headers["Last-Modified"] = current_time + " GMT"
                response.status_code = 200
                return response

    response = flask.Response(json.dumps({"error": library_name + " not found"}), mimetype='application/json')
    response.status_code = 404
    return response
